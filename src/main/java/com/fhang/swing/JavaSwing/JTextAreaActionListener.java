/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.fhang.swing.JavaSwing;

;
import javax.swing.*;
;
import java.awt.event.*;


/**
 *
 * @author Natthakritta
 */


public class JTextAreaActionListener implements ActionListener{

    JLabel label1, label2;
    JTextArea area;
    JButton b;

    JTextAreaActionListener() {
        JFrame rame = new JFrame();
        label1 = new JLabel();
        label1.setBounds(50, 25, 100, 30);
        label2 = new JLabel();
        label2.setBounds(160, 25, 100, 30);
        area = new JTextArea();
        area.setBounds(20, 75, 250, 200);
        b = new JButton("Count Words");
        b.setBounds(100, 300, 120, 30);
        b.addActionListener(this);
        rame.add(label1);
        rame.add(label2);
        rame.add(area);
        rame.add(b);
        rame.setSize(450, 450);
        rame.setLayout(null);
        rame.setVisible(true);
    }

    public void actionPerformed(ActionEvent e) {
        String text = area.getText();
        String words[] = text.split("\\s");
        label1.setText("Words: " + words.length);
        label2.setText("Characters: " + text.length());
    }

    public static void main(String[] args) {
        new JTextAreaActionListener();
    }
}
