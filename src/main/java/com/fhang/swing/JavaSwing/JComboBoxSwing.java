/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.fhang.swing.JavaSwing;

/**
 *
 * @author Natthakritta
 */
import javax.swing.*;

public class JComboBoxSwing {

    JComboBoxSwing() {
        JFrame frame = new JFrame();
        String ipad[] = {"IpadPro", "IpadMini", "IpadAir"};
        JComboBox cb = new JComboBox(ipad);
        cb.setBounds(50, 50, 90, 20);
        frame.add(cb);
        frame.setLayout(null);
        frame.setSize(400, 500);
        frame.setVisible(true);
    }

    public static void main(String[] args) {
        new JComboBoxSwing();
    }
}
