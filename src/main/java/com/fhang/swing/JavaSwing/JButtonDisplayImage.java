/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.fhang.swing.JavaSwing;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;

/**
 *
 * @author Natthakritta
 */
public class JButtonDisplayImage {

    public JButtonDisplayImage() {
        JFrame frame = new JFrame("Button");
        frame.setSize(300, 400);
        frame.setLayout(null);
        JButton Button = new JButton
        (new ImageIcon("C:\\Users\\Natthakritta\\Downloads\\IMG_4053.PNG"));
        Button.setBounds(100, 100, 100, 40);
        frame.add(Button);
        frame.setVisible(true);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    }

    public static void main(String[] args) {
        new JButtonDisplayImage();
    }
}
