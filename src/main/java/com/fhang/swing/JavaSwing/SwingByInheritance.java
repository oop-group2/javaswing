/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.fhang.swing.JavaSwing;

import javax.swing.JButton;
import javax.swing.JFrame;

/**
 *
 * @author Natthakritta
 */
public class SwingByInheritance extends JFrame {

    JFrame frame;
    JButton button;

    SwingByInheritance() {
        button = new JButton("click");
        button.setBounds(130, 100, 100, 40);

        add(button);
        setSize(400, 400);
        setLayout(null);
        setVisible(true);

    }

    public static void main(String[] args) {
        new SwingByInheritance();
    }

}
