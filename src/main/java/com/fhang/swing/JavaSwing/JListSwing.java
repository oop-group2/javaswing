/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.fhang.swing.JavaSwing;

/**
 *
 * @author Natthakritta
 */
import javax.swing.*;

public class JListSwing {

    JListSwing() {
        JFrame frame = new JFrame();  
        DefaultListModel<String> label1 = new DefaultListModel<>();  
          label1.addElement("Item1");  
          label1.addElement("Item2");  
          label1.addElement("Item3");  
          label1.addElement("Item4");  
          JList<String> list = new JList<>(label1);  
          list.setBounds(100,100, 75,75);  
          frame .add(list);  
          frame .setSize(400,400);  
          frame .setLayout(null);  
          frame .setVisible(true);  
    }
    
    public static void main(String[] args) {
        new JListSwing();
    }
}
