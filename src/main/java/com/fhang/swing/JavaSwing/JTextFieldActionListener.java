/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.fhang.swing.JavaSwing;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JTextField;

/**
 *
 * @author Natthakritta
 */
public class JTextFieldActionListener  implements ActionListener{

    JTextField textf1, textf2, textf3;
    JButton button1, button2;

    
    JTextFieldActionListener() {
        JFrame frame = new JFrame();
        frame.setSize(300, 300);
        frame.setLayout(null);
        frame.setVisible(true);
        textf1 = new JTextField();
        textf3 = new JTextField();
        textf3 = new JTextField();
        textf1.setBounds(50, 50, 150, 20);
        textf2.setBounds(50, 100, 150, 20);
        textf3.setBounds(50, 150, 150, 20);
        textf3.setEditable(false);
        frame.add(textf1);
        frame.add(textf2);
        frame.add(textf3);

        button1 = new JButton("+");
        button2 = new JButton("-");
        button1.setBounds(50, 200, 50, 50);
        button2.setBounds(120, 200, 50, 50);
        button1.addActionListener(this);
        button2.addActionListener(this);
        frame.add(button1);
        frame.add(button2);
    }

    public static void main(String[] args) {
        new JTextFieldActionListener();
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        String s1=textf1.getText();  
        String s2=textf2.getText();  
        int a=Integer.parseInt(s1);  
        int b=Integer.parseInt(s2);  
         int c=0;  
        if(e.getSource()==button1){  
            c=a+b;  
        }else if(e.getSource()==button2){  
            c=a-b;  
        }  
        String result=String.valueOf(c);  
        textf3.setText(result);  
    }
}
