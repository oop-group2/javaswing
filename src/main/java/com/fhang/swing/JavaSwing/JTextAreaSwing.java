/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.fhang.swing.JavaSwing;

import javax.swing.JFrame;
import javax.swing.JTextArea;

/**
 *
 * @author Natthakritta
 */
public class JTextAreaSwing {

    public static void main(String[] args) {
        new JTextAreaSwing();
        JFrame frame = new JFrame();
        JTextArea area = new JTextArea("Hello welcome to JTextArea");
        area.setBounds(10, 30, 200, 50);
        frame.add(area);
        frame.setSize(300, 300);
        frame.setLayout(null);
        frame.setVisible(true);
    }
}
