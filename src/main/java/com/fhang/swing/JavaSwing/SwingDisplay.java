/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.fhang.swing.JavaSwing;

/**
 *
 * @author Natthakritta
 */
import java.awt.*;
import javax.swing.JButton;
import javax.swing.JFrame;

public class SwingDisplay extends Canvas {

    public void paint(Graphics g) {
        Toolkit t = Toolkit.getDefaultToolkit();
        Image i = t.getImage("C:\\Users\\Natthakritta\\Downloads\\5.gif");
        g.drawImage(i, 120, 100, this);
        
    }

    public static void main(String[] args) {
        SwingDisplay m = new SwingDisplay();
        JFrame f = new JFrame();
        JButton button = new JButton("when I sit and do my homework");
        button.setBounds(250, 50, 300, 30);
        f.add(button);
        f.add(m);
        f.setSize(700, 500);
        f.setVisible(true);
    }
}
