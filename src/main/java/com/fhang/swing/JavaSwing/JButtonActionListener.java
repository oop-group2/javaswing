/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.fhang.swing.JavaSwing;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JTextField;

/**
 *
 * @author Natthakritta
 */
public class JButtonActionListener {

    public static void main(String[] args) {
        JFrame farme = new JFrame("Button Example");
        final JTextField textf = new JTextField();
        textf.setBounds(50, 100, 150, 20);
        JButton button = new JButton("Click Here");
        button.setBounds(50, 50, 95, 30);
        button.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                textf.setText("Welcome to Java");
            }
        });
        farme.add(button);
        farme.add(textf);
        farme.setSize(400, 400);
        farme.setLayout(null);
        farme.setVisible(true);
    }
}
