/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.fhang.swing.JavaSwing;

import javax.swing.*;

/**
 *
 * @author Natthakritta
 */
public class JCheckBoxSwing {

    JCheckBoxSwing() {
        JFrame frame= new JFrame("CheckBox");  
        JCheckBox checkBox1 = new JCheckBox("C++");  
        checkBox1.setBounds(100,100, 50,50);  
        JCheckBox checkBox2 = new JCheckBox("Java", true);  
        checkBox2.setBounds(100,150, 100,50);  
        frame.add(checkBox1);  
        frame.add(checkBox2);  
        frame.setSize(400,400);  
        frame.setLayout(null);  
        frame.setVisible(true);  
     
    }

    public static void main(String[] args) {
        new JCheckBoxSwing();
    }
}
