/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.fhang.swing.JavaSwing;

/**
 *
 * @author Natthakritta
 */
import javax.swing.*;
import java.awt.event.*;

public class JRadioBurronAction extends JFrame implements ActionListener {

    JRadioButton radio1, radio2;
    JButton button;

    JRadioBurronAction() {
        radio1 = new JRadioButton("Male");
        radio1.setBounds(100, 50, 100, 30);
        radio2 = new JRadioButton("Female");
        radio2.setBounds(100, 100, 100, 30);
        ButtonGroup bg = new ButtonGroup();
        bg.add(radio1);
        bg.add(radio2);
        button = new JButton("click");
        button.setBounds(100, 150, 80, 30);
        button.addActionListener(this);
        add(radio1);
        add(radio2);
        add(button);
        setSize(300, 300);
        setLayout(null);
        setVisible(true);
    }

    public static void main(String[] args) {
        new JRadioBurronAction();
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (radio1.isSelected()) {
            JOptionPane.showMessageDialog(this, "You are Male.");
        }
        if (radio2.isSelected()) {
            JOptionPane.showMessageDialog(this, "You are Female.");
        }
    }
}
