/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.fhang.swing.JavaSwing;

import javax.swing.*;
import java.awt.event.*;

/**
 *
 * @author Natthakritta
 */
public class JCheckBoxItemListener {

    JCheckBoxItemListener() {
        JFrame farme = new JFrame("CheckBox");
        final JLabel label = new JLabel();
        label.setHorizontalAlignment(JLabel.CENTER);
        label.setSize(400, 100);
        JCheckBox checkbox1 = new JCheckBox("C++");
        checkbox1.setBounds(150, 100, 50, 50);
        JCheckBox checkbox2 = new JCheckBox("Java");
        checkbox2.setBounds(150, 150, 100, 50);
        farme.add(checkbox1);
        farme.add(checkbox2);
        farme.add(label);
        checkbox1.addItemListener(new ItemListener() {
            public void itemStateChanged(ItemEvent e) {
                label.setText("C++ Checkbox: "
                        + (e.getStateChange() == 1 ? "checked" : "unchecked"));
            }
        });
        checkbox2.addItemListener(new ItemListener() {
            public void itemStateChanged(ItemEvent e) {
                label.setText("Java Checkbox: "
                        + (e.getStateChange() == 1 ? "checked" : "unchecked"));
            }
        });
        farme.setSize(400, 400);
        farme.setLayout(null);
        farme.setVisible(true);
    }

    public static void main(String[] args) {
        new JCheckBoxItemListener();
    }
}
