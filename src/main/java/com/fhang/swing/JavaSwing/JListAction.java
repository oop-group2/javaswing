/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.fhang.swing.JavaSwing;

/**
 *
 * @author Natthakritta
 */
import javax.swing.*;
import java.awt.event.*;

public class JListAction {

    JListAction() {
        JFrame f = new JFrame();
        final JLabel label = new JLabel();
        label.setSize(500, 100);
        JButton b = new JButton("Show");
        b.setBounds(200, 150, 80, 30);
        final DefaultListModel<String> l1 = new DefaultListModel<>();
        l1.addElement("C");
        l1.addElement("C++");
        l1.addElement("Java");
        l1.addElement("PHP");
        final JList<String> list1 = new JList<>(l1);
        list1.setBounds(100, 100, 75, 75);
        f.add(list1);
        f.add(b);
        f.add(label);
        f.setSize(450, 450);
        f.setLayout(null);
        f.setVisible(true);

        b.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                String data = "";
                if (list1.getSelectedIndex() != -1) {
                    data = "Programming language Selected: " + list1.getSelectedValue();
                    label.setText(data);
                }

                label.setText(data);
            }
        });
    }

    public static void main(String[] args) {
        new JListAction();
    }
}
