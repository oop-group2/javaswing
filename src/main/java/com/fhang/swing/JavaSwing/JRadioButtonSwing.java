package com.fhang.swing.JavaSwing;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author Natthakritta
 */
import javax.swing.*;

public class JRadioButtonSwing {

    JRadioButtonSwing() {
        JFrame frame = new JFrame();
        JRadioButton radio1 = new JRadioButton("A) Male");
        JRadioButton radio2 = new JRadioButton("B) Female");
        radio1.setBounds(75, 50, 100, 30);
        radio2.setBounds(75, 100, 100, 30);
        ButtonGroup bg = new ButtonGroup();
        bg.add(radio1);
        bg.add(radio2);
        frame.add(radio1);
        frame.add(radio2);
        frame.setSize(300, 300);
        frame.setLayout(null);
        frame.setVisible(true);
    }

    public static void main(String[] args) {
        new JRadioButtonSwing();
    }
}
