/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.fhang.swing.JavaSwing;

/**
 *
 * @author Natthakritta
 */
import javax.swing.*;
import java.awt.event.*;

public class JComboBoxAction {

    JComboBoxAction() {
        JFrame frame = new JFrame();
        final JLabel label = new JLabel();
        label.setHorizontalAlignment(JLabel.CENTER);
        label.setSize(400, 100);
        JButton button = new JButton("Show");
        button.setBounds(200, 100, 75, 20);
        String Ipad[] = {"IpadPro", "IpadMini", "IpadAir"};
        final JComboBox cb = new JComboBox(Ipad);
        cb.setBounds(50, 100, 90, 20);
        frame.add(cb);
        frame.add(label);
        frame.add(button);
        frame.setLayout(null);
        frame.setSize(350, 350);
        frame.setVisible(true);
        button.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                String data = "Ipad Selected: "
                        + cb.getItemAt(cb.getSelectedIndex());
                label.setText(data);
            }
        });
    }

    public static void main(String[] args) {
        new JComboBoxAction();
    }
}
